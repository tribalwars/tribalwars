<h3>New item</h3>
<br />
<br />
<p>While examining the surroundings, the paladin found the following item:
<br /><br />
<i>{$lang->grab("knight", $itemname)|regex_replace:'/\'/':'&rsquo;'}</i>
<br /><br />
Through further research, the following properties of the item were found:
<br /><br />
<i>{$lang->grab("knight_items_des", "$itemname")|regex_replace:'/\'/':'&rsquo;'}</i>
<br /><br />
The paladin can be equipped with the item in the weapons room.
<br /><br />
<a href="game.php?screen=statue&mode=inventory">For a weapons room</a></p>
<br />